#!/bin/sh

if [ "$1" == "" ];then
    echo ""
    echo "usage: 03_diffusion_pipeline.sh <subjectFolder>"
    echo ""
    echo "   e.g. 03_diffusion_pipeline.sh CMO-001-001"
    echo ""
    exit 1
fi
pipedir=/well/win-fmrib-analysis/users/omg004/bb_pipeline_v_2.5
declare -A scanner
scanner["001"]=prisma
scanner["002"]=skyra

# helper functions
get_site(){
    echo $1 | sed s/"-"/" "/g | awk '{print $2}'
}
get_coeff_file(){
    s=${scanner[$1]}
    coeff=$pipedir/bb_data/bb_GDC_coeff_${s}.grad
    echo $coeff
}


subjectFolder=$1

if [ ! -d $subjectFolder ];then
    echo "Cannot find subject $subjectFolder" 
    exit 1
fi

source $pipedir/activate_pipeline.sh

script_folder=/users/win-fmrib-analysis/omg004/scripts/pipeline

site=`get_site $subjectFolder`
coeff=`get_coeff_file $site`

echo "Running site $site"
echo "Coeff file: $coeff"

# TODO: 
# - create log dir (or just use the logdir the fidel creates)
# - call fsl_sub. each job waits for the previous one. log into log dir
$script_folder/cmore_diff.sh $subjectFolder $coeff
$script_folder/cmore_tbss_md.sh $subjectFolder
$script_folder/cmore_tbss_md_NAWM.sh $subjectFolder
