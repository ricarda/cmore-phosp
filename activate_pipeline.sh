#!/bin/bash



. /well/win-fmrib-analysis/users/omg004/bb_pipeline_v_2.5/bb_python/bb_python/bin/activate
source /well/win-fmrib-analysis/users/omg004/bb_pipeline_v_2.5/init_vars
export BB_BIN_DIR="/well/win-fmrib-analysis/users/omg004/bb_pipeline_v_2.5"
export PATH="/well/win/software/packages/fsl_sub/bin/:$BB_BIN_DIR/bb_ext_tools/freesurfer4/bin/:$BB_BIN_DIR/bb_ext_tools/freesurfer4//fsfast/bin/:$BB_BIN_DIR/bb_ext_tools/freesurfer4/tktools/:$BB_BIN_DIR/bb_ext_tools/freesurfer4/mni/bin/:/well/win/projects/ukbiobank/fbp/bb_FSL/bin/:/well/win/projects/ukbiobank/scripts/:$BB_BIN_DIR//bb_python/bb_python/bin/:$BB_BIN_DIR//bb_pipeline_tools/:$BB_BIN_DIR//bb_general_tools/:/well/win/projects/ukbiobank/fbp/autodownload/:/mgmt/uge/8.6.8/bin/lx-amd64/:/sbin/:/bin/:/usr/sbin/:/usr/bin/:/usr/lpp/mmfs/bin/"
export PYTHONPATH="/well/win-fmrib-analysis/users/omg004/bb_pipeline_v_2.5"
