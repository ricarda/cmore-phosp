#!/bin/sh

if [ "$1" == "" ];then
    echo ""
    echo "usage: 04_run_swi.sh  <subjectFolder> "
    echo ""
    echo "   e.g. 04_run_swi.sh CMO-001-089"
    echo ""
    exit 1
fi

subjectFolder=$1

# Obtain study info and scanner name
study=`echo $subjectFolder |sed s/"-"/" "/g | awk '{print $2}'`

# Create SWI_TOTAL files
datadir=/users/win-fmrib-analysis/omg004/cmore/subj

for im in MAG PHA;do
    #if [ `$FSLDIR/bin/imtest $datadir/$study/$subjectFolder/SWI/SWI_TOTAL_${im}` -eq 0 ];then
    mv $datadir/$study/$subjectFolder/SWI/${im}_TE1/*A.nii.gz $datadir/$study/$subjectFolder/SWI/unclassified
    file=`echo $datadir/$study/$subjectFolder/SWI/${im}_TE1/3DGRE_SWI_COILHE1-4*nii.gz | awk '{print $1}' | sed s/".nii.gz"/""/g` 
    imcp $file $datadir/$study/$subjectFolder/SWI/SWI_TOTAL_${im}
    cp ${file}.json $datadir/$study/$subjectFolder/SWI/SWI_TOTAL_${im}.json
    #fi
    #if [ `$FSLDIR/bin/imtest $datadir/$study/$subjectFolder/SWI/SWI_TOTAL_${im}_TE2` -eq 0 ];then
    mv $datadir/$study/$subjectFolder/SWI/${im}_TE1/*A.nii.gz $datadir/$study/$subjectFolder/SWI/unclassified
    file=`echo $datadir/$study/$subjectFolder/SWI/${im}_TE2/3DGRE_SWI_COILHE1-4*nii.gz | awk '{print $1}' | sed s/".nii.gz"/""/g`
    imcp $file $datadir/$study/$subjectFolder/SWI/SWI_TOTAL_${im}_TE2
    cp ${file}.json $datadir/$study/$subjectFolder/SWI/SWI_TOTAL_${im}_TE2.json
    #fi
done

# swi_reg
scanner=`cat /users/win-fmrib-analysis/omg004/scripts/pipeline/sites_info.txt | grep $study | awk '{print tolower($6)}'`
coeff_file=$BB_BIN_DIR/bb_data/bb_GDC_coeff_${scanner}.grad
if [ ! -f $coeff_file ];then
    # default coeff file. not sure this is a good thing to do
    coeff_file=$BB_BIN_DIR/bb_data/bb_GDC_coeff.grad
fi
$BB_BIN_DIR/bb_structural_pipeline/bb_swi_reg $subjectFolder $coeff_file


QSMdir=/users/win-fmrib-analysis/omg004/scripts/pipeline/UKB_QSM_pipeline_Oct2021
matlabdir=/apps/well/matlab/2019a 
pythondir=/well/miller/users/gdy716/Python/anaconda2
subjdir=/well/win-biobank/projects/imaging/data/data3/PHOSP/PHOSP_12_10_21/cmore/subj/$study/$subjectFolder
fsldir=/well/win/software/packages/fsl/6.0.3
mask=brain_mask
ne=2
TE1=9.42
TE2=20
GDC=SWI_TOTAL_MAG_orig_ud_warp #bb_GDC_coeff_prisma.grad

$QSMdir/UKB_QSM_MAIN.sh -QSMdir $QSMdir \
    -fsldir $fsldir \
    -matlabdir $matlabdir \
    -pythondir $pythondir \
    -subjdir $subjdir \
    -mask $mask \
    -ne $ne \
    -TE1 $TE1 -TE2 $TE2 \
    -GDC $GDC 
