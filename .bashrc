# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions

### WIN config
MODULEPATH=$MODULEPATH:/well/win/software/modules
export MODULEPATH

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/gpfs3/users/win-fmrib-analysis/omg004/google-cloud-sdk/path.bash.inc' ]; then . '/gpfs3/users/win-fmrib-analysis/omg004/google-cloud-sdk/path.bash.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/gpfs3/users/win-fmrib-analysis/omg004/google-cloud-sdk/completion.bash.inc' ]; then . '/gpfs3/users/win-fmrib-analysis/omg004/google-cloud-sdk/completion.bash.inc'; fi

# Python
module load Python/3.7.4-GCCcore-8.3.0
source ~/py/bin/activate

export FSLDIR="/well/win/projects/ukbiobank/fbp/bb_FSL"
