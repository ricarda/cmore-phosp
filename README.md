# CMORE-PHOSP

If you start from the dicoms, run scripts 00_..., 01_..., and 03_... after one another. 


- **00_pre_fidel.py** creates folders with DICOMs separately for each modality.

_Usage: python 00_pre_fidel.py --subjectFolderName_ (also accepts multiple subjects, or, if you are in the subject directory, <subject*>)

- **01_dcm2niii.sh** converts the DICOMs and outputs the nifti and .json files into each subject's folder. It also takes one example dicom per modality and creates .txt files in the DICOM directory that -amongst other things- contain the COG info.

_Usage: ./01_dcm2niii.sh sujectFolderName site_number_
(As there are data from multiple sites my structure is .../site/subject_folder/...)

- **02_bb_pipeline_queue.sh** uses .../bb_pipeline_tools/**bb_pipeline_queue.py (modified on Nov, 4th)** as entry point to the BB pipeline (which now allows you to skip modalities).

_Usage: ./02_bb_pipeline_queue.sh subjectFolderName_


- **activate_pipeline.sh**, **env_vars**, and **init_vars** are all located **in** the **main bb_pipeline_v_2.5 directory**.
**bb_pipeline_queue.py** is located in the **bb_pipeline_tools sub-directory** (in my case in /users/win-fmrib-analysis/omg004/scratch/bb_pipeline_v_2.5/bb_pipeline_tools; bb_file_manager_fnc.py in the same directory also has been modified to accommodate CMORE-style reduced DWI data).**
- Other changes to **bb_file_manager_fnc.py** (located **in** bb_pipeline_v_2.5/**bb_pipeline_tools**/) accommodate deviations in SWI protocols from the usual UK Biobank protocol (it now ignores "expected_num_files = (2 * echoes_SWI * coils_SWI) + num_total_MAGs + num_total_PHAs").
- The **naming_pattern_CMO-00x.json** and **ideal_config_sizes_CMO-00x.json** are expected to be **in the bb_data sub-directory** of the main bb_pipeline directory (in my case bb_pipeline_v_2.5).


