#!/usr/bin/env python
import argparse

parser = argparse.ArgumentParser()
   
parser.add_argument('--subjects', required=True, help="list of subjects",nargs='*')

args = parser.parse_args()


import sys
from pydicom import dcmread
from glob import glob
import os
import numpy as np

# OUTPUT FOLDER
O='/well/win-biobank/projects/imaging/data/data3/PHOSP/PHOSP_12_10_21/cmore/subj/001'
# WHERE THE DICOM DATA RESIDES
D='/well/win-biobank/projects/imaging/data/data3/PHOSP/PHOSP_12_10_21/cmore/'


# -------- THINGS TO BE CHANGED ----------- #

# subject ID
# maybe turn into a loop over sujects

#subjects = ["CMO-001-100", "CMO-001-101", "CMO-001-102", "CMO-001-103", "CMO-001-105", "CMO-001-106", "CMO-001-107", "CMO-001-108", "CMO-001-109", "CMO-001-110", "CMO-001-111", "CMO-001-112", "CMO-001-113", "CMO-001-114", "CMO-001-116", "CMO-001-117", "CMO-001-118", "CMO-001-119", "CMO-001-120", "CMO-001-121", "CMO-001-123", "CMO-001-124", "CMO-001-125", "CMO-001-129", "CMO-001-131"]

subjects = args.subjects

#["CMO-002-001", "CMO-002-002", "CMO-002-003", "CMO-002-004", "CMO-002-005", "CMO-002-006", "CMO-002-007", "CMO-002-008", "CMO-002-009", "CMO-002-010", "CMO-002-011", "CMO-002-012", "CMO-002-013", "CMO-002-014", "CMO-002-015", "CMO-002-016", "CMO-002-017", "CMO-002-018", "CMO-002-019", "CMO-002-020", "CMO-002-021", "CMO-002-022", "CMO-002-023", "CMO-002-024", "CMO-002-025", "CMO-002-026", "CMO-002-027", "CMO-002-028", "CMO-002-029", "CMO-002-030", "CMO-002-031", "CMO-002-032", "CMO-002-033", "CMO-002-034", "CMO-002-035", "CMO-002-036", "CMO-002-037", "CMO-002-038", "CMO-002-039", "CMO-002-040", "CMO-002-042", "CMO-002-043", "CMO-002-044", "CMO-002-045", "CMO-002-046", "CMO-002-047", "CMO-002-048", "CMO-002-049", "CMO-002-050", "CMO-002-051", "CMO-002-052", "CMO-002-053", "CMO-002-054", "CMO-002-055", "CMO-002-056", "CMO-002-057", "CMO-002-058", "CMO-002-059", "CMO-002-060", "CMO-002-061", "CMO-002-062", "CMO-002-063", "CMO-002-064", "CMO-002-065", "CMO-002-066", "CMO-002-067", "CMO-002-069", "CMO-002-070", "CMO-002-071", "CMO-002-072", "CMO-002-073", "CMO-002-074", "CMO-002-075", "CMO-002-076", "CMO-002-078", "CMO-002-079", "CMO-002-080", "CMO-002-081", "CMO-002-083", "CMO-002-084", "CMO-002-085"]
 
for subj in subjects:
#subj='CMO-001-099'
    print(subj)
    
# ------------------------------------------- #

# CREATE THE FOLDER IF NOT THERE
    if not os.path.exists(O):
        os.mkdir(O)

# LIST OF DICOMS FOUND FOR THE SUBJECT
    dicoms=sorted(glob(os.path.join(D,subj,'*','*.dcm')))

# Series descriptions based on CMORE dicom data distribution doc
    brain_files = {
        'T1' : ['t1_mprage_sag_p2_iso', 'MPRAGE_1mm_sag'],
        'dMRI' : ['cmrr_mbep2d_diff','ep2d_diff_3scantr_revPE','DWI','DWI_rev_PE','ep2d_diff_3scantr_b1000'],
        'SWI' : ['3dgre_swi','SWI'],
        'T2' : ['t2_space_dark-fluid_sag_p3', 'T2_FLAIR_2mm'],
        'ASL' : ['to_ep2d_VEPCASL','pCASL_'],
       }


# LIST OF THE MODALITIES
    modalities=['T1','dMRI','SWI','T2','ASL']
    mod_dicoms = {}
    for m in modalities:
        mod_dicoms[m] = []

# MOD UKBB CODES AND ASSOCIATED MODALITIES
    fidel_numbers =          [ '16', '18',   '19',  '20', '66']
    keys          = np.array([ 't1', 'diff', 'swi', 't2', 'ASL'])
    keys_mods     = dict(zip(keys,modalities))
    x             = dict(zip(modalities,fidel_numbers))

# CREATE NUMBERED FOLDERS
    for f in fidel_numbers:
        directory=os.path.join(O,subj,'DICOM',f)
        if not os.path.exists(directory):
            os.makedirs(directory)

# LINK INDIVIDUAL DICOM FILES TO NUMBERED FOLDERS

    SD = []
    ntotal=len(dicoms)
    for i,d in enumerate(dicoms):
        print(f'processing dicom files {i+1}/{ntotal}',end='\r')
        f = os.path.basename(d)

    # read in series description
        sd=dcmread(d).SeriesDescription
        SD.append(sd)

        for fn,bf in zip(fidel_numbers,brain_files):
            for regexp in brain_files[bf]:
                if regexp in sd:
                    odir=os.path.join(O,subj,'DICOM',fn)
                    os.system(f'ln -s {d} {odir}/{f}')
                    #print(f'Found {regexp}. Linking to {fn}|{bf}')
    print('Done.')





# --------------------------------------------------------------
