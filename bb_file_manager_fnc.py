#!/bin/env python

'''
 Authors: Fidel Alfaro Almagro
 FMRIB, Oxford University
 8-Jan-2021 
 Version 1.0
 ProjectDir=
 '''
# Change 
# S.Jbabdi 10/2021
# - commented out code that checks if dMRI has more than one volume.

import os
import re
import json
import copy
import nibabel as nib
from   shutil import copyfile
import bb_general_tools.bb_path as bb_path
import bb_pipeline_tools.bb_logging_tool as LT

logger      = None
idealConfig = None
fileConfig  = None

def init_fmf(in_logger, in_idealConfig, in_fileConfig):
    global logger
    global idealConfig
    global fileConfig
    
    if logger is None: 
        logger = in_logger
    else:
        raise RuntimeError("Module logger has already been set.")

    if idealConfig is None: 
        idealConfig = in_idealConfig
    else:
        raise RuntimeError("Module idealConfig has already been set.")
    
    if fileConfig is None: 
        fileConfig = in_fileConfig
    else:
        raise RuntimeError("Module fileConfig has already been set.")

def formatFileConfig():
    result=""    
    for key in fileConfig:
        result=result + key + "\n"
        for value in fileConfig[key]:
            result=result + "  "  + value + "\n"
    return result

def generate_SBRef(origPath, outputPath):
    commandToRun=os.environ['BB_BIN_DIR']+'/bb_functional_pipeline/bb_generate_SBRef ' + origPath + " " + outputPath
    logger.warn("There was no SBRef data for the subject " + origPath) 
    logger.warn("The SBRef data will be generated now using the middle point of the subject")
    logger.warn("Command to run: " + commandToRun )
    LT.runCommand(logger, commandToRun )     

def remove_phase_info(fileName):
    result=re.sub('_[PH]|_[ph].', '',fileName)
    return result

def remove_coil_info(fileName):
    result=re.sub('_COIL[0-9]*_', '_',fileName)
    return result

def remove_echo_info(fileName):
    result=re.sub('_ECHO.*_','_',fileName)
    return result

def rename_no_coil_echo_info(fileName):
    result=remove_coil_info(fileName)
    result=remove_echo_info(result)
    
    if (fileName != result):
        move_file(fileName, result)
    
    return result

def read_json(fileName):
    
    result={}

    if os.path.isfile(fileName):
        if bb_path.isImage(fileName):
            jsonFileName=bb_path.removeImageExt(fileName)+ '.json'
                
            if os.path.isfile(jsonFileName):
                with open(jsonFileName, 'r') as f:
                    result=json.load(f)
    return result

def get_image_json_field(fileName, field):
    
    result=[]
    jsonDict=read_json(fileName)

    if jsonDict != {}:
        result = jsonDict[field]
    
    return result


#def save_acquisition_date_time(fileName):
#    dateTime=get_image_json_field(fileName, 'AcquisitionDateTime')
#    #20140831122443.796875
#    #Format this date    

def image_type_contains(fileName, desiredType):

    imageType=get_image_json_field(fileName, 'ImageType')

    # 2 possible formats in the BIDS json file
    if isinstance(imageType, str):
        imageType=imageType.split('_')
    elif not isinstance(imageType, list):
        raise NameError('The content of the json file associated with ' + fileName + ' is incorrect')
    
    if desiredType in imageType:
        return True
 
    return False

def is_normalised(fileName):
    return image_type_contains(fileName, 'NORM')

def is_phase(fileName):
    return image_type_contains(fileName, 'P')

def is_phase_SWI(fileName):
    return (image_type_contains(fileName, 'P') or 
            bb_path.removeImageExt(fileName).endswith('_PH') or
            bb_path.removeImageExt(fileName).endswith('_REAL') or
            bb_path.removeImageExt(fileName).endswith('_IMAGINARY'))

def is_not_norm_mag(fileName, check_normalisation):
    if check_normalisation:
        isNorm = is_normalised(fileName)
    else:
        isNorm = False

    isPhase = is_phase_SWI(fileName)

    return (not isNorm) and (not isPhase)

def get_SWI_TE(fileName):
    TE = [int(x.replace('ECHO','')) for x in fileName.split('_') if x.startswith('ECHO')]

    if TE != []:
        return TE[0]
    else:
        return 1

def get_complex_phase(fileName):
    if bb_path.removeImageExt(fileName).endswith('_REAL'):
        return '_REAL'
    elif bb_path.removeImageExt(fileName).endswith('_IMAGINARY'):
        return '_IMAGINARY'
    else:
        return ''

def move_to(listFiles, destination):
    for fileName in listFiles:
        move_file(fileName, destination+fileName)



# Convert all the file names to upper case to avoid 
# ambiguities (Extensions are always in lower case)
# Remove _ character at the beginning of the filename
def capitalize_and_clean(listFiles):

    logger.info('File names changed to upper case.')
    for fileName in listFiles:

        newFileName=fileName.replace(',','_').upper()
        newFileName=newFileName.replace('COILHEA_HEP','COIL')
        if newFileName.startswith("_"):
            newFileName=newFileName[1:]

        endings=[".NII.GZ", "BVAL", "BVEC", "JSON"]
        
        for ending in endings:
            if newFileName.endswith(ending):
                newFileName=newFileName.replace(ending, ending.lower())

        os.rename(fileName,newFileName)

def move_file(oldPath, newPath):
    
    #The file may be a json file and may have been moved previously
    if os.path.isfile(oldPath):
        logger.info("File moved/renamed: " + oldPath + " to " + newPath)
        os.rename(oldPath, newPath)

        #If there is an associated json, move it as well
        if bb_path.isImage(oldPath):
            plainOrigName=bb_path.removeImageExt(oldPath)
            plainNewName=bb_path.removeImageExt(newPath)

            if os.path.isfile(plainOrigName + '.json'):
                os.rename(plainOrigName + '.json', plainNewName + '.json')

def cp_file(oldPath, newPath):
    #The file may be a json file and may have been moved previously
    if os.path.isfile(oldPath):
        logger.info("File moved/renamed: " + oldPath + " to " + newPath)
        copyfile(oldPath, newPath)

        #If there is an associated json, move it as well
        if bb_path.isImage(oldPath):
            plainOrigName=bb_path.removeImageExt(oldPath)
            plainNewName=bb_path.removeImageExt(newPath)

            if os.path.isfile(plainOrigName + '.json'):
                copyfile(plainOrigName + '.json', plainNewName + '.json')

def move_file_add_to_config(oldPath, key, boolAppend):
    if boolAppend:
        move_file(oldPath, idealConfig[key] + "/" +oldPath)
        fileConfig[key].append(idealConfig[key] + "/" +oldPath)
    else:
        move_file(oldPath, idealConfig[key])
        fileConfig[key]=idealConfig[key]

def cp_file_add_to_config(oldPath, key, boolAppend):
    if boolAppend:
        cp_file(oldPath, idealConfig[key] + "/" +oldPath)
        fileConfig[key].append(idealConfig[key] + "/" +oldPath)
    else:
        cp_file(oldPath, idealConfig[key])
        fileConfig[key]=idealConfig[key]

def robustSort(listFiles):
    listFiles.sort()
    finalList=copy.copy(listFiles)
    altern=[]

    for fileName in listFiles:
        
        rest=fileName[fileName.rfind("_")+1:]
        
        try:
            cadNumb=rest[:rest.find(".")]
            if cadNumb[-1] == "A":
                cadNumb=cadNumb[:-1]
            for phaseEnd in ['PH', 'ph', 'Imaginary', 'imaginary', 'IMAGINARY',
                             'Real', 'real', 'REAL']:            
                if cadNumb == phaseEnd:
                    rest=fileName[fileName.rfind("_"  +phaseEnd)-2:]
                    cadNumb=rest[:rest.find("_" + phaseEnd + ".")]
            
            numb=int(cadNumb)
            altern.append(numb)
        except ValueError:
            logger.error("Found a file with an improper name: " + fileName + " . It will be moved to the unclassified folder")
            finalList.remove(fileName)

    newList=[x for (y,x) in sorted(zip(altern,finalList))]

    return newList

# The flag parameter indicates whether this is T1 or T2_FLAIR
# Some processings should not check for the normalised versions
def manage_struct(listFiles, flag, check_normalisation):
    
    listFiles=robustSort(listFiles)
    numFiles=len(listFiles)

    listFiles=[rename_no_coil_echo_info(x) for x in listFiles]

    logger.info("List of files received for struct processing: " + str(listFiles))
    logger.info("Check normalisation flag: " + str(check_normalisation))

    if check_normalisation:
        boolNorm=[is_normalised(x) for x in listFiles]
    else:
        boolNorm=[True for x in listFiles]
    
    logger.info("Normalisation state per file: " + str(boolNorm))

    if not any(boolNorm):
        logger.error("There was not an intensity-normalized " + flag + ".")
        if flag == "T1":        
            logger.error("It will not be possible to process the subject")
    else:
        indexLastNorm=(numFiles - list(reversed(boolNorm)).index(True)) -1
        normalisedFileName=listFiles[indexLastNorm]
        move_file_add_to_config(normalisedFileName, flag, False)
        listFiles.remove(normalisedFileName)
        boolNorm=boolNorm[:indexLastNorm]

        if False in boolNorm:

            indexLastNotNorm=(len(boolNorm) - list(reversed(boolNorm[:indexLastNorm])).index(False)) -1
            
            if indexLastNotNorm>=0:
                notNormalisedFileName=listFiles[indexLastNotNorm]  
                move_file_add_to_config(notNormalisedFileName, flag + "_notNorm", False)
                listFiles.remove(notNormalisedFileName)
 
    for fileName in listFiles:
        move_file(fileName, 'unclassified/'+fileName) 
# The flag parameter indicates whether this is T1 or T2_FLAIR
def manage_ASL(listFiles, check_normalisation):
    
    listFiles = [rename_no_coil_echo_info(x) for x in listFiles]
    mainFiles = [x for x in listFiles if ("MOCO" not in x)]
    mainFiles = robustSort(mainFiles)

    if len(mainFiles) != 1:
        logger.error(str("There should be just one ASL image (not MOoCo)"))
    else:
        move_file_add_to_config(mainFiles[0], "ASL", False)

    # Move remaining files to the unclassified directory
    for fil in listFiles:
        if os.path.isfile(fil):
            move_file(fil, 'ASL/unclassified/' + fil)
        

# The flag parameter indicates whether this is resting or task fMRI
def manage_fMRI(listFiles, flag):
    
    listFiles=robustSort(listFiles)

    logger.info("List of files received for fMRI processing: " + str(listFiles))

    numFiles=len(listFiles)
    dim=[]

    listFiles=[rename_no_coil_echo_info(x) for x in listFiles]

    # Get the dimensions for all the fMRI images
    for fileName in listFiles:
        epi_img=nib.load(fileName)
        dim.append (epi_img.get_header()['dim'][4])

    if numFiles == 0 :
        logger.warn("There was no " + flag + "FMRI data")
        
    elif numFiles == 1:
        # If the only fMRI we have is the SBRef
        if dim[0] == 1:
            logger.error("There was only SBRef data for the subject. There will be no " + flag + "fMRI processing")
            move_file_add_to_config(listFiles[0], flag + "_SBRef", False)

        # If we have fMRI data but no SBRef, we generate it.         
        else:
            move_file_add_to_config(listFiles[0], flag, False)
            generate_SBRef(idealConfig[flag], idealConfig[flag + "_SBRef"])
            fileConfig[flag + "_SBRef"]=idealConfig[flag + "_SBRef"]

    elif numFiles == 2:
        biggestImageDim=max(dim)
        indBiggestImage=dim.index(biggestImageDim)
        indSmallestImage=1 - indBiggestImage
        
        # If there is at least one propper fMRI image
        if biggestImageDim > 1:
            move_file_add_to_config(listFiles[indBiggestImage], flag, False)

            # If the other image is an SBRef image
            if dim[indSmallestImage] == 1:
                move_file_add_to_config(listFiles[indSmallestImage], flag + "_SBRef", False)
            
            # If not, forget about it and generate and SBRef
            else:
                generate_SBRef(idealConfig[flag], idealConfig[flag + "_SBRef"])
                fileConfig[flag + "_SBRef"]=idealConfig[flag + "_SBRef"]

        else:
            logger.error("There was only SBRef data for the subject. There will be no " + flag + "fMRI processing")
            move_file_add_to_config(listFiles[numFiles -1], flag + "_SBRef", False)

    # If there are more than 2 rfMRI images, and at least one has more than one volume, 
    # we will take the biggest one as the fMRI volume and generate take as SBRef the one
    # with the previous numeration. If that one is not a proper SBRef, generate it.
    elif (max(dim) > 1):
        indBiggestImage=dim.index(max(dim))
        move_file_add_to_config(listFiles[indBiggestImage], flag, False)

        
        fileName=listFiles[indBiggestImage]
        plainFileName=bb_path.removeImageExt(fileName)
        number=int(plainFileName.split("_")[-1])
        
        ind=-1

        for fileToCheck in listFiles:
            #Check if the file with the previous numeration is in the list
            numberToCheck=int(bb_path.removeImageExt(fileName).split("_")[-1])
            if numberToCheck == (number-1):
                ind=listFiles.index(fileToCheck)    

        
        # If there is a file with the file number that should correspond to this case
        if ind >0:
            # If the file with the previous numeration is a SBREF file
            if dim[ind] == 1:
                move_file_add_to_config(listFiles[ind], flag + "_SBRef", False)

            # If not, forget about it and generate a new one
            else:
                generate_SBRef(idealConfig[flag], idealConfig[flag + "_SBRef"])
                fileConfig[flag + "_SBRef"]=idealConfig[flag + "_SBRef"]     

        else:
            generate_SBRef(idealConfig[flag], idealConfig[flag + "_SBRef"])
            fileConfig[flag + "_SBRef"]=idealConfig[flag + "_SBRef"]

    # There are several fMRI images but neither of them have more than one volume  
    else:
        logger.error("There was only SBRef data for the subject. There will be no " + flag + "fMRI processing.")
        move_file_add_to_config(listFiles[numFiles -1], flag + "_SBRef", False)


def manage_DWI(listFiles, inverted_PED, B_files):

    listFiles=robustSort(listFiles)

    logger.info("List of files received for dMRI processing: " + str(listFiles))
    logger.info("Inverted Phase Encoding Direction for dMRI data: " + str(inverted_PED))

    numFiles=len(listFiles)
    listFiles=[rename_no_coil_echo_info(x) for x in listFiles]

    subListFilesD={}
    imageFilesD={}

    if numFiles == 0:
        logger.error("There was no DWI data.  There will be no DWI processing.")        

    else:
        #errorFound=False
        encodingDirections=["PA", "AP"]
        if inverted_PED:
            encodingDirectionsCode={"PA": "j-" , "AP": "j"}
        else:
            encodingDirectionsCode={"PA": "j" , "AP": "j-"}
        
        #Code needed for the inconsistency in the file names in Diffusion over the different phases
        if listFiles[0].startswith("MB3"):

            subListFilesD['PA']=[x for x in listFiles if x.find('PA') != -1]
            imageFilesD['PA']=[x for x in subListFilesD['PA'] if bb_path.isImage(x)]

            subListFilesD['AP']=[x for x in listFiles if x not in subListFilesD['PA']]
            imageFilesD['AP']=[x for x in subListFilesD['AP'] if bb_path.isImage(x)]

            logger.info('Files in direction PA: ' + str(imageFilesD['PA']))
            logger.info('Files in direction AP: ' + str(imageFilesD['AP']))

        #else:
        #    for direction in encodingDirections:
        #        subListFilesD[direction]=[x for x in listFiles if x.find(direction) != -1]
        #        imageFilesD[direction]=[x for x in subListFilesD[direction] if bb_path.isImage(x)]
        else:
            images = [x for x in listFiles if bb_path.isImage(x)]
            for direction in encodingDirections:
                imageFilesD[direction] = []
                for image in images:
                    baseName = bb_path.removeImageExt(image)
                    enc = get_image_json_field(image, 'PhaseEncodingDirection')
                    if enc == encodingDirectionsCode[direction]:
                        imageFilesD[direction].append(image)
                        subListFilesD[direction] = [x for x in listFiles if x.startswith(baseName)]
                logger.info('Files in direction ' + direction + ':' + 
                            str(subListFilesD[direction]))
        try:

            for direction in encodingDirections:

               dim=[]
               subListFiles=subListFilesD[direction]
               imageFiles=imageFilesD[direction]

               for fileName in imageFiles:
                   epi_img=nib.load(fileName)
                   dim.append(epi_img.get_header()['dim'][4])

               numImageFiles=len (imageFiles)

               if numImageFiles == 0 :
                   raise Exception("There should be at least one DWI image in the " + direction + 
                                   " direction with more than one volume. DWI data is not correct."
                                   " There will be no diffusion processing.")

               biggestImageDim=max(dim)
               indBiggestImage=dim.index(biggestImageDim)

               # There is no proper DWI image
               #if biggestImageDim <=1:
               #    raise Exception("There should be at least one DWI image in the " + direction + 
               #                    " direction with more than one volume. DWI data is not correct."
               #                    " There will be no diffusion processing.")

               if numImageFiles>1:
                   # Check if there is SBRef file for the direction
                   if (dim.count(1) == 0):
                        logger.warn("There was no SBRef file in the " + direction + " direction.")

                   # If there is at least one, take the last one.                    
                   else:

                       # Get the index of the last image with dimension = 1
                       indexSBRef=numImageFiles - list(reversed(dim)).index(1) -1
                       move_file_add_to_config(imageFiles[indexSBRef], direction + "_SBRef", False)
      

               # Take the biggest image in the selected direction and set it as the DWI image for that direction
               move_file_add_to_config(imageFiles[indBiggestImage], direction , False)

               # If bval and bvec files are not manually entered used the generated ones
               if B_files == "" :
                   # BVAL and BVEC files should have the same name as the image, changing the extension
                   bvalFileName=bb_path.removeImageExt(imageFiles[indBiggestImage]) + ".bval"
                   bvecFileName=bb_path.removeImageExt(imageFiles[indBiggestImage]) + ".bvec"

                   if (not bvalFileName in subListFiles) or (not bvecFileName in subListFiles):
                       raise Exception("There should be 1 bval and 1 bvec file in " + direction + 
                                       " direction. DWI data is not correct. There will be no"
                                       " diffusion processing.")

                   move_file_add_to_config(bvecFileName, direction + "_bvec", False)
                   move_file_add_to_config(bvalFileName, direction + "_bval", False)
               else:
                   cp_file_add_to_config(B_files + '/' + direction + '.bvec' , 
                                           direction + "_bvec", False)
                   cp_file_add_to_config(B_files + '/' + direction + '.bval', 
                                           direction + "_bval", False)
    

        # In case of any big error in the data, set DWI data as inexistent.
        except Exception as e:
            for key in ["AP", "AP_bval", "AP_bvec", "PA", "PA_bval", "PA_bvec" ]:
                fileConfig[key]=""
            logger.error(str(e))

        # Set the rest of the files as unclassified
        for fileName in listFiles:
            if os.path.isfile(fileName):
                move_file(fileName, "unclassified/"+fileName)


def manage_SWI(listFiles, check_normalisation, coils_SWI, echoes_SWI, complex_phase):

    listFiles=robustSort(listFiles)
    numFiles=len(listFiles)

    logger.info("List of files received for SWI processing: " + str(listFiles))
    logger.info("Check normalisation for SWI processing: " + str(check_normalisation))
    logger.info("Number of coils for SWI processing: " + str(coils_SWI))
    logger.info("Number of echo tmes for SWI processing: " + str(echoes_SWI))
    logger.info("Complex phase for SWI processing: " + str(complex_phase))

    with open('SWI/num_coils.txt', 'w') as f:
        f.write(str(coils_SWI))
    with open('SWI/num_ET.txt', 'w') as f:
        f.write(str(echoes_SWI))
    with open('SWI/complex_phase.txt', 'w') as f:
        if complex_phase:
            f.write("1")
        else:
            f.write("0")

    try:
        coils_SWI = int(coils_SWI)
    except ValueError:
        logger.warning("Eror in the specified number of SWI coils: " + coils_SWI + '. Will use default: 31')
        coils_SWI = 32

    try:
        echoes_SWI = int(echoes_SWI)
    except ValueError:
        logger.warning("Eror in the specified number of SWI echoes: " + echoes_SWI + '. Will use default: 2')
        echoes_SWI = 2

    if check_normalisation:
        num_total_MAGs = echoes_SWI * 2
    else:
        num_total_MAGs = echoes_SWI 

    if complex_phase:
        num_total_PHAs = 2 * echoes_SWI
    else:
        num_total_PHAs = echoes_SWI


    #TODO: Find all files with actual coil info
    expected_num_files = (2 * echoes_SWI * coils_SWI) + num_total_MAGs + num_total_PHAs
    # ignore the above
    expected_num_files = numFiles

    if numFiles != expected_num_files:
        logger.error("There should be " + str(expected_num_files) + " SWI files. " + 
                     str(numFiles) + " found. There will be no SWI processing")

    else:
        mainFiles=[x for x in listFiles if (("_COIL_" in x) or ("_COILRM_" in x))]

        #numMainFiles=len(mainFiles)
        
        for mainFile in mainFiles:
            listFiles.remove(mainFile)

        for i in range(echoes_SWI):
            key = 'SWI_MAG_TE' + str(i+1)
            fileConfig[key]=[]        
            key = 'SWI_PHA_TE' + str(i+1)
            fileConfig[key]=[]        

        # Classifying coil files
        for fileName in listFiles:

            boolPhase=is_phase_SWI(fileName)
            TE = get_SWI_TE(fileName)

            if boolPhase:
                move_file_add_to_config(fileName, "SWI_PHA_TE" + str(TE), True)                
            else:
                move_file_add_to_config(fileName, "SWI_MAG_TE" + str(TE), True)                


        # Classifying main SWI files
        # is_phase function does not work due to SWI acquisition not complying with standard
        # DICOM configuration and hence, dcm2niix does not get the phase properly
        notNormMagFiles = [mainFile for mainFile in mainFiles if is_not_norm_mag(mainFile, check_normalisation)]

        if check_normalisation and len(notNormMagFiles) != echoes_SWI:
            logger.warn("There should be " + str(echoes_SWI) +  
                        " not normalised SWI files. SWI data will not be processed")
            for mainFile in mainFiles:
                if os.path.isfile(mainFile):
                    move_file(mainFile, "SWI/unclassified/"+mainFile)

        else:
            if check_normalisation: 
                for fileName in notNormMagFiles:
                    TE = get_SWI_TE(fileName)

                    if TE == 1:
                        move_file_add_to_config(fileName, 
                                                "SWI_TOTAL_MAG_notNorm", False)
                    else:
                        move_file_add_to_config(fileName, 
                                    "SWI_TOTAL_MAG_notNorm_TE" + str(TE), False)

                    mainFiles.remove(fileName)

            for mainFile in mainFiles:
                TE = get_SWI_TE(mainFile)
                boolPhase=is_phase_SWI(mainFile)

                if not boolPhase:
                    if TE == 1:
                        move_file_add_to_config(mainFile, "SWI_TOTAL_MAG", False)
                    else:
                        move_file_add_to_config(mainFile, "SWI_TOTAL_MAG_TE" + str(TE), False)
                else:
                    cp = get_complex_phase(mainFile)
                    if TE == 1:
                        move_file_add_to_config(mainFile, "SWI_TOTAL_PHA" + cp, False)
                    else:
                        move_file_add_to_config(mainFile, "SWI_TOTAL_PHA" + cp + "_TE" + str(TE), False)

    # Move remaining files to the unclassified directory
    for fil in listFiles:
        if os.path.isfile(fil):
            move_file(fil, 'SWI/unclassified/' + fil)
