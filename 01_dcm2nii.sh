#!/bin/sh

#. $BB_BIN_DIR/bb_pipeline_tools/bb_set_header


if [ "$1" == "" ];then
    echo "usage: ./01_dcm2niii.sh <sujectID> <site_number>"
    exit 1
fi
BB_BIN_DIR=/well/win-fmrib-analysis/users/omg004/bb_pipeline_v_2.5
#datadir=/well/win-fmrib-analysis/users/omg004/TMP
datadir=/well/win-biobank/projects/imaging/data/data3/PHOSP/PHOSP_12_10_21/cmore/subj/$2
subjectID=$1

#if [ "$2" == "" ] ; then
#    visit="2"
#else
#    visit="$2"
#fi



origDir=`pwd`

cd $datadir/$subjectID/DICOM


for dicomPackage in  16 18 19 20 66; do
    echo $dicomPackage
    $HOME/bin/dcm2niix -b y -z y -m o -f %p_coil%a_echo%e_%2s -o ../ $dicomPackage/ > ./log4 # 2>&1
done

namT1=`ls    --color=none 16/*.dcm | sort | head -n 1`
#namtfMRI=`ls --color=none 17/*.dcm | sort | head -n 1`
namdMRI=`ls  --color=none 18/*.dcm | sort | head -n 1`
namSWI=`ls   --color=none 19/*.dcm | sort | head -n 1`
namT2=`ls    --color=none 20/*.dcm | sort | head -n 1`
#namrfMRI=`ls --color=none 25/*.dcm | sort | head -n 1`
namASL=`ls   --color=none 66/*.dcm | sort | head -n 1`

if [ ! "$namT1" == "" ] ; then
    cp $namT1 ../T1.dcm
    cat ../T1.dcm | strings > ../T1_strings.txt
    python $BB_BIN_DIR/bb_general_tools/bb_read_dicom_header.py  -f ../T1.dcm --all > ../T1.txt
fi

if [ ! "$namtfMRI" == "" ] ; then
    cp $namtfMRI ../tfMRI.dcm
    cat ../tfMRI.dcm | strings > ../tfMRI_strings.txt
    python $BB_BIN_DIR/bb_general_tools/bb_read_dicom_header.py  -f ../tfMRI.dcm --all > ../tfMRI.txt
fi

if [ ! "$namdMRI" == "" ] ; then
    cp $namdMRI ../dMRI.dcm
    cat ../dMRI.dcm | strings > ../dMRI_strings.txt
    python $BB_BIN_DIR/bb_general_tools/bb_read_dicom_header.py  -f ../dMRI.dcm --all > ../dMRI.txt
fi

if [ ! "$namSWI" == "" ] ; then
    cp $namSWI ../SWI.dcm
    cat ../SWI.dcm | strings > ../SWI_strings.txt
    python $BB_BIN_DIR/bb_general_tools/bb_read_dicom_header.py  -f ../SWI.dcm --all > ../SWI.txt
fi

if [ ! "$namT2" == "" ] ; then
    cp $namT2 ../T2.dcm
    cat ../T2.dcm | strings > ../T2_strings.txt
    python $BB_BIN_DIR/bb_general_tools/bb_read_dicom_header.py  -f ../T2.dcm --all > ../T2.txt
fi

if [ ! "$namrfMRI" == "" ] ; then
    cp $namrfMRI ../rfMRI.dcm
    cat ../rfMRI.dcm | strings > ../rfMRI_strings.txt
    python $BB_BIN_DIR/bb_general_tools/bb_read_dicom_header.py  -f ../rfMRI.dcm --all > ../rfMRI.txt
fi

if [ ! "$namASL" == "" ] ; then
    cp $namASL ../ASL.dcm
    cat ../ASL.dcm | strings > ../ASL_strings.txt
    python $BB_BIN_DIR/bb_general_tools/bb_read_dicom_header.py  -f ../ASL.dcm --all > ../ASL.txt
fi

cd ../
#rm -r DICOM/*
mv *.txt *.dcm DICOM/

#cd ..

#bb_file_manager.py $subjectID
#bb_basic_QC.py $subjectID

#mv $t/$subjectID $origDir/
#cd $origDir

#. $BB_BIN_DIR/bb_pipeline_tools/bb_set_footer
