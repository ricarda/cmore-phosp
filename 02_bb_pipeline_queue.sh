#!/bin/sh

##################################################################################################################################
#This is step 3) - using bb_pipeline_tools/bb_pipeline_queue.py as entry point to the BB pipeline. Run this after                #
#1) /users/win-fmrib-analysis/omg004/scripts/pipeline/00_pre_fidel.py (creates folders with separate DICOMs for each modality) and  #
#2) /users/win-fmrib-analysis/omg004/scripts/pipeline/01_dcm2nii.sh                                                              #
##################################################################################################################################

if [ "$1" == "" ];then
    echo ""
    echo "usage: 02_bb_pipeline_queue.sh <subjectFolder> "
    echo ""
    echo "   e.g. 02_bb_pipeline_queue.sh CMO-001-001"
    echo ""
    exit 1
fi

subjectFolder=$1

# Obtain study info and scanner name
study=`echo $subjectFolder |sed s/"-"/" "/g | awk '{print $2}'`
scanner=`cat /users/win-fmrib-analysis/omg004/scripts/pipeline/sites_info.txt | grep $study | awk '{print tolower($6)}'`

pipedir=/well/win-fmrib-analysis/users/omg004/bb_pipeline_v_2.5


namingPatterns=$pipedir/bb_data/naming_pattern_CMO-${study}.json
basic_QC_file=$pipedir/bb_data/ideal_config_sizes_CMO-${study}.json
coeff_file=$pipedir/bb_data/bb_GDC_coeff_${scanner}.grad
if [ ! -f $coeff_file ];then
    # default coeff file. not sure this is a good thing to do
    coeff_file=$pipedir/bb_data/bb_GDC_coeff.grad
fi


if [ ! -f $namingPatterns ];then
    echo " Cannot find naming pattern file : $namingPatterns"
    exit 1
fi
if [ ! -f $basic_QC_file ];then
    echo " Cannot find basic_QC_file : $basic_QC_file"
    exit 1
fi


pipedir=/well/win-fmrib-analysis/users/omg004/bb_pipeline_v_2.5
#pipedir=/well/win/projects/ukbiobank/fbp/bb_pipeline_CO
source $pipedir/activate_pipeline.sh

#create bvals/bvecs file for CMORE-PHOSP (3 principal directions only; same-ish for all subjects)
#D=/users/win-fmrib-analysis/omg004/scratch/TMP/bvals_bvecs
D=/well/win-biobank/projects/imaging/data/data3/PHOSP/PHOSP_12_10_21/cmore/subj/$study
echo "0 1000 1000 1000" > $D/AP.bval
echo "0 1 0 0"  > $D/AP.bvec
echo "0 0 1 0" >> $D/AP.bvec
echo "0 0 0 1" >> $D/AP.bvec
echo "0"> $D/PA.bval
echo "0 "  > $D/PA.bvec
echo "0 " >> $D/PA.bvec
echo "0 " >> $D/PA.bvec

cd /well/win-biobank/projects/imaging/data/data3/PHOSP/PHOSP_12_10_21/cmore/subj/$study

$BB_BIN_DIR/bb_pipeline_tools/bb_pipeline_queue.py --namingPatterns $namingPatterns \
    --basic_QC_file $basic_QC_file \
    -B $D $subjectFolder  -S 1 \
    --coeff $coeff_file --cmore_diff --skip_modalities func asl
